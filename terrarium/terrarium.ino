#include <dht.h> // DHTStable by Rob Tillaart
#include <WiFi.h>

#define TEMP 0
#define HUM 1
#define PIR 2
#define LIGHT 3

/************ WIFI INFORMATION (CHANGE THESE FOR YOUR SETUP) ******************/

#define wifi_ssid "" //type your WIFI information inside the quotes
#define wifi_password ""

/**************************** PIN DEFINITIONS ********************************************/

#define DHTPIN      13
#define LIGHT_PIN   15 //in1
#define HUM_PIN     16 //in2
#define FAN_PIN     17 //in3
#define DHTTYPE     
DHT22

bool lightOn = false;
bool humidifierOn = false;
bool fanOn = false;

int lightOnDuration = 30000; //ms
int lightOffDuration = 60000; //ms

int lastLightInstant = 0;
int lastHumidifierInstant = 0;
int lastFanInstant = 0;


int humidifierOnDuration = 5000; //ms
int humidifierOffDuration = 5000; //ms

int fanOnDuration = 1000; //ms
int fanOffDuration = 2000; //ms



/**************************** GLOBAL VARIABLE DEFINITIONS *******************************************/

String db_ip_address = "";
String db_access_token = "";

float diffTEMP = 0.5;
float tempValue = 0.0;

float diffHUM = 1;
float humValue;

dht DHT;

/**************************** SETUP *******************************************/

void setup() {

  Serial.begin(9600);
  
  pinMode(DHTPIN, INPUT);
  pinMode(LIGHT_PIN, OUTPUT);
  pinMode(HUM_PIN, OUTPUT);
  pinMode(FAN_PIN, OUTPUT);
  
  digitalWrite(LIGHT_PIN, HIGH);
  digitalWrite(HUM_PIN, HIGH);
  digitalWrite(FAN_PIN, HIGH);
  
  delay(10);
  Serial.print("Connecting to ");
  Serial.println(wifi_ssid);
  
  WiFi.mode(WIFI_STA);
  WiFi.begin(wifi_ssid, wifi_password);
  
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
  }
  
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  
  Serial.println("Ready");
  }
  
  
  bool checkBoundSensor(float newValue, float prevValue, float maxDiff) {
    return newValue < prevValue - maxDiff || newValue > prevValue + maxDiff;
  }
  
  
  void sendState(char sensor){
  String url = "http://"+db_ip_address+"/apps/api/2/devices/";
  switch (sensor){
      case TEMP:
      Serial.println("sendState called with TEMP");
      url += "/setTemperature/"+(String)((int)tempValue);
      break;
      case HUM:
      Serial.println("sendState called with HUM");
      url += "/setHumidity/"+(String)humValue;
      break;
      default:
      Serial.println("sendState called with a bad case");
  }
}

/**************************** LOOP *******************************************/

void loop() {

  int chk = DHT.read22(DHTPIN);
  float newTempValue = DHT.temperature;
  float newHumValue = DHT.humidity;
  newTempValue = (newTempValue/5)*9+32;
  
  if (checkBoundSensor(newTempValue, tempValue, diffTEMP)) {
      tempValue = newTempValue;
      //sendState(TEMP);
      Serial.print("Sending temp value: ");
      Serial.println(tempValue);
      delay(100);
  }
  
  if (checkBoundSensor(newHumValue, humValue, diffHUM)) {
      humValue = newHumValue;
      //sendState(HUM);
      Serial.print("Sending humidity value: ");
      Serial.println(humValue);
      delay(100);
  }

  // timers
  unsigned long now = millis();
  unsigned long timeDiff;
  
  digitalWrite(LIGHT_PIN, !lightOn);
  digitalWrite(HUM_PIN, !humidifierOn);
  digitalWrite(FAN_PIN, !fanOn);
  
  // Light Timers
  if (lightOn == true){
      timeDiff = now - lastLightInstant;
      if (timeDiff > lightOnDuration){
          lightOn = false;
          lastLightInstant = now;
      }
  } else { // lightOn == false
      timeDiff = now - lastLightInstant;
      if (timeDiff > lightOffDuration){
          lightOn = true;
          lastLightInstant = now;
      }
  }
  
  // Humidifier Timers
  if (humidifierOn == true){
      timeDiff = now - lastHumidifierInstant;
      if (timeDiff > humidifierOnDuration){
          humidifierOn = false;
          lastHumidifierInstant = now;
      }
  } else { // humidifierOn == false
      timeDiff = now - lastHumidifierInstant;
      if (timeDiff > humidifierOffDuration){
          humidifierOn = true;
          lastHumidifierInstant = now;
      }
  }
  
  // Fan Timers
  if (fanOn == true){
      timeDiff = now - lastFanInstant;
      if (timeDiff > fanOnDuration){
          fanOn = false;
          lastFanInstant = now;
      }
  } else { // fanOn == false
      timeDiff = now - lastFanInstant;
      if (timeDiff > fanOffDuration){
          fanOn = true;
          lastFanInstant = now;
      }
  }



delay(1000);
}
